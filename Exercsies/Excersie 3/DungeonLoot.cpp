#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>

using namespace std;

struct Item
{
	string name;
	int gold;
};

vector<Item*> randomItemGenerator()
{
	vector<Item*> Items;
	Item *item0 = new Item{ "Cursed Stone", 0 };
	Item *item1 = new Item{ "Jellopy", 5 };
	Item *item2 = new Item{ "Thick Leather", 25 };
	Item *item3 = new Item{ "Sharp Talon", 50 };
	Item *item4 = new Item{ "Mithril Ore", 100 };		

	Items.push_back(item4);
	Items.push_back(item3);
	Items.push_back(item2);
	Items.push_back(item1);
	Items.push_back(item0);

	Items[rand() & Items.size()];

	return Items;

}

void printInventory(vector<Item*> inventory)
{
	for (int i = 0; i < inventory.size(); i++)
	{
		Item* currentItem = inventory[i];
		cout << currentItem->name << " | " << currentItem->gold << "\n";
	}
}

int main()
{
	srand(unsigned int(time(NULL)));

	vector<Item*> loot = randomItemGenerator();
	printInventory(loot);

}