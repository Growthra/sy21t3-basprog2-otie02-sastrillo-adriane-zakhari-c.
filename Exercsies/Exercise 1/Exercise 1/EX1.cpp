#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int diceRolls()
{
	int roll1, roll2;
	srand((unsigned int (time(NULL))));

	roll1 = rand() % 6 + 1;
	roll2 = rand() % 6 + 1;
	int diceSum = roll1 + roll2;	
	
	cout << "\nDie One: " << roll1;
	cout << "\nDie Two: " << roll2;
	cout << "\nTotal: " << diceSum << "\n";
	cout << "=======================\n";
	return diceSum;
}
int playerRolls()
{
		int playerArray[1];
		for (int i = 0; i < 1; i++)
		{
			int playerRoll = diceRolls();
			playerArray[i] = playerRoll;
			cout << playerRoll << endl;
			return playerRoll;
		}
		
}
int aiRolls()
{
	int aiArray[1];
	for (int i = 0; i < 1; i++)
	{
		int aiRoll = diceRolls();
		aiArray[i] = aiRoll;
		cout << aiRoll << endl;
		return aiRoll;
	}
	

}

int bettingAndChecking(int bet, int money)
{
	if (bet > money || bet < 0)
	{
		cout << "invalid amount" << "\n";
	}
	else
	{
		cout << "\nYou betted: " << bet;
		cout << "\nUpdated current funds: " << money << "\n";
	}

	return money;
}
int betScreen()
{
	int totalMoney = 1000;
	int bet;
	cout << "Your currents funds: " << totalMoney;
	cout << "\nHow much will you bet: ";
	cin >> bet;
	int sub = totalMoney - bet;
	bettingAndChecking(bet, sub);
	cout << "\n";
	system("pause");
	system("cls");
	return bet;
}
void payoutConditions()
{
	int playerRollResults = playerRolls();
	int aiRollResults = aiRolls();
	int moneyModifier = betScreen();

	if (playerRollResults == 2 && aiRollResults == 2)
	{
		cout << "Draw\n";
	}
	if (playerRollResults == 2)
	{
		cout << "\nWOW! SNAKE EYES\n";
		moneyModifier * 3;
	}
	if (playerRollResults > aiRollResults)
	{
		cout << "You the player won this round!\n";
	}
	else
	{
		cout << "The AI won this round!\n";
	}
}

void playRound()
{
	betScreen();
	char r;
	cout << "\nEnter r to roll dice: ";
	cin >> r;
	if (r)
	{
		cout << "\nPlayer will now roll...";
		diceRolls();
	}
	else 
	{
		cout << "ivalid input";
	}
	system("pause");
	cout << "\n The AI will now roll...\n";
	diceRolls();
	system("pause");
	cout << "\n\n";
	payoutConditions();

}
int main()
{
	playRound();
	cout << "\n\n";
	system("pause");
	return 0;
}