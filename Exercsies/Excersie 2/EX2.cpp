#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>

using namespace std;

void filler(vector <string> &inventory)
{
	int maxSize = 10;
	string items[4] = { "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };

	for (int i = 0; i < maxSize; i++)
	{
		inventory.push_back(items[rand() % 4]);
	}
}

void printInventory(const vector<string> &inventory)
{
	for (unsigned int i = 0; i < inventory.size(); i++)
	{
		if (i < 1) { cout << "\nInventory: " << inventory[i];}
		else { cout << ", " << inventory[i];}
	}
	cout << "\n";
}

void dupelicateChecker(vector<string> inventory)
{
	unsigned int i, j, dupe = 0, found = 0;
	string items[4] = { "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };
	for (i = 0; i < inventory.size(); i++)
	{
		for (j = 0; j < inventory.size(); j++)
			{ if (inventory[i] == inventory[j]) { dupe++;} }
				if (inventory[i] == items[i]) { found = 1; break; }
	}
	if (found == 1)
	{ cout << "You have " << dupe << " " << items[i] << " inside your inventory.\n"; }

	else
	{ cout << items[i] << " does not exist in the array.\n"; }
}

int main()
{
	srand(unsigned int(time(NULL)));
	vector<string> inventory;
	filler(inventory);
	printInventory(inventory);
	dupelicateChecker(inventory);
}