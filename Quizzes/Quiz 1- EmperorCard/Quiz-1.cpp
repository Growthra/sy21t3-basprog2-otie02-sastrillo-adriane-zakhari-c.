#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;


void deckBuilder(int &round)
{
	vector<string> emperorDeck;
	vector<string> slaveDeck;
	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
	{
		emperorDeck.push_back("Emperor\n");
		for (int i = 0; i < 4; i++)
		{
			emperorDeck.push_back("Citizen\n");
		}
		for (int j = 0; j < emperorDeck.size(); j++)
		{
			cout << " " << emperorDeck[j];
		}
	}
	else if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12)
	{
		slaveDeck.push_back("Slave\n");
		for (int i = 0; i < 4; i++)
		{
			slaveDeck.push_back("Citizen\n");
		}
		for (int j = 0; j < slaveDeck.size(); j++)
		{
			cout << " " << slaveDeck[j];
		}
		
	}
}


string kanjiPlay()
{
	string  user;
	cout << "\n==============\n";
	cout << "   Kanji Turn   ";
	cout << "\n==============\n";
	cout << "\nWhat card would you like to play? \n" << "Type the title of the card that you wish to play:";
	cin >> user;
	return user;
}
vector<string> tokogawaPlay(int &round)
{
	vector<string> ai;
	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
	{
		ai.push_back("Slave");
		ai.push_back("Citizen");
	}
	else if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12)
	{
		ai.push_back("Emperor");
		ai.push_back("Citizen");
	}
	cout << "\n\n===============\n";
	cout << "  Tokogawa Turn   ";
	cout << "\n===============\n";
	cout << "\nTokogawa Played: " << ai[rand() % 2]<< "\n\n";
	return ai;
}

int matchEvaluation(string card1, string card2)
{
	int result = 0;
	if ((card1 == "Citizen") && (card2 == "Citizen"))
	{
		result = 0;
	}
	else if (((card1 == "Emperor") && (card2 == "Citizen")) || ((card1 == "Slave") && (card2 == "Emeperor")) || ((card1 == "Citizen") && (card2 == "Slave")))
	{
		result = 1;
	}
	else if (((card2 == "Emperor") && (card1 == "Citizen")) || ((card2 == "Slave") && (card1 == "Emperor")) || ((card2 == "Citizen") && (card2 == "Slave")))
	{
		result = 2;
	}
	return result;
}


int bet;
int prizeControl(int bet, int round)
{
	unsigned int yen;
	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9) {

		yen = bet * 100000;
	}
	if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12) {

		yen = bet * 500000;
	}
	return yen;
}
bool betCheck(int bet, int mmLeft)
{
	bool flag;
	if (bet <= mmLeft) {
		flag = true;
	}
	else if (bet > mmLeft) {
		flag = false;
	}
	return flag;
}
void printWager(int round, int mmLeft, int moneyEarned)
{
	cout << "\n\n===============================================\n";
	cout << "                ROUND " << round;
	cout << "\n===============================================\n";
	cout << "How much would you like to bet, in milimeters?\n";
	cout << "(You still have " << mmLeft << " milimeters left.)\n";
	cout << "Enter: ";
	cin >> bet;
	betCheck(bet, mmLeft);
	if (betCheck(bet, mmLeft) == true)
	{
		cout << "\nif you win you'll get " << prizeControl(bet, round) << "Cash.\n\n";
		cout << "Current Funds: " << moneyEarned << "\n\n";
	}
	else if (betCheck(bet, mmLeft) == false)
	{
		cout << "\n:!INvALID BET!:\n" << ":!SYSTEM EREOR = OVER-BETTED!:\n:!NIDDLE PUNCTURE WILL BEGIN!:\n" << ":!YOU DIED!:\n";
		exit(0);
	}
}

void endGame(int round, int moneyEarned, int mmleft, int bet)
{
	if (round <= 12 && moneyEarned == 20000000 && betCheck(bet, mmleft) == true)
	{
		cout << "You did not entirely win! You only got " << moneyEarned << " Yen in 12 rounds!" << endl;
		system("pause");
		exit(0);
	}
	else if (round == 12 && moneyEarned < 20000000 && betCheck(bet, mmleft) == false) {

		cout << "You won! You got" << moneyEarned << " Yen at Round " << round << endl;
	}
}
void playRound(int &round, int mmLeft, int moneyEarned)
{
	
	printWager(round, mmLeft, moneyEarned);

	deckBuilder(round);
	kanjiPlay();
	tokogawaPlay(round);	

	system("pause");
	system("cls");
}


int main()
{

	
	srand(unsigned int(time(NULL)));

	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;

	
	while (round < 13)
	{
		playRound(round, mmLeft, moneyEarned);
		round++;
	}

	cout << "\n\n";
	return 0;
}